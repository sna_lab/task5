FROM python:3.7

EXPOSE 8080

COPY app.py /app/app.py
CMD ["python3","/app/app.py", "8080"]
