import sys
import socket


def parse_arguments():
	# get cli arguments
	if len(sys.argv) != 2:
	    print('The program should be executed with 1 argument (port number): "python3 app.py 1234"')
	    exit(1)
	if not sys.argv[1].isdigit():
	    print('The 1st argument (port number) must be an integer')
	    exit(1)
	return int(sys.argv[1])

if __name__ == '__main__':
	port = parse_arguments()
	
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.bind(('', port))  # bind to localhost
	
	print(f'Server started at "localhost:{port}"')
	
	buf_size = 1024
	
	try:
		while True:
			msg, address = s.recvfrom(buf_size)
			msg = msg.decode().strip()
			print(f'({address}) Got message: "{msg}"')
			answer = f'Got your message: "{msg}"'
			s.sendto(answer.encode(), address)
	except KeyboardInterrupt:
		print('Server is stopping...')
		if s:
			s.close()
		print('Server has stopped')
